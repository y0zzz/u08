terraform {
  required_providers {
    linode = {
      source = "linode/linode"
      version = "2.9.7"
    }
  }
}

provider "linode" {
  token = "c2a943f77f33e4914cc8d7a4e4ffc06b1ece6ca566eab4c41ea74dd0d28b31a3"
}

resource "linode_instance" "instance1" {
  label            = "caprover1"
  type             = "g6-nanode-1"
  region           = "se-sto"
  authorized_keys  = [ "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC8x4TyuVsw32x/qXf91429XuTEr+lbqWnjuI1+oOG6WS8kZ7Q/UW7FrU413Oq/VhV+bhNM6rq4NNf/SlUfvUQ2YcwvyiUx95CmUGbhvXZmaIgVFG8sVRT80MeMRumWTOSWbeAVWzqiKZCbQIRY6vomI0ncon7aMUooNc+cIyF14rJTIczPMSz9QTz5v+ZTu/MZB45I7+wIDDT0orHkHF1mdSm2KHlI50/KTI65dyzP5naZph1ngSf5WIJXSNg/Klu6bHZHhETPQGiUk/sIaOIOF7xTc6dT79PcYFVDniQlaR13izcNUBDEMv/vdclcFs1Grs5FwD38hreM/FuSmDRBqojGq/w8B1H7F3/CnmYitQDCuDRxgXAKMw+CvI371UzDgzrmdvN21aQHSkeILJvGja2KivgEc3eWNE9t3NZFVEw4ImGk6svSVcRYSiqJDXmzdtrjmHBDbGcVoKBpAmSEtsbO+lwiliEzOuNAAQZEOJTu0AV0HOQBKNfe/GlpNcrxgjL+t+vD/Berjw8sk6WAym/rbAPNcS2fzBAiXfbAGAEKTafLasZmw8cp9mpFlT0g1wxNRoiaXcaLum2Si9w7qcVL/VJgThZkwY9AWX2IGO3LWIylwRNeSsG0+jPPG0KSWNT2tWJWTdZGwPNOTBOz2y+OoSFeLjPIhEujdkaX3w== " ]
  root_pass        = "GIHmc5J2xIv2exb"
  image            = "linode/ubuntu22.04"
  private_ip       = true
}

resource "linode_instance" "instance2" {
  label            = "caprover2"
  type             = "g6-nanode-1"
  region           = "se-sto"
  authorized_keys  = [ "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC8x4TyuVsw32x/qXf91429XuTEr+lbqWnjuI1+oOG6WS8kZ7Q/UW7FrU413Oq/VhV+bhNM6rq4NNf/SlUfvUQ2YcwvyiUx95CmUGbhvXZmaIgVFG8sVRT80MeMRumWTOSWbeAVWzqiKZCbQIRY6vomI0ncon7aMUooNc+cIyF14rJTIczPMSz9QTz5v+ZTu/MZB45I7+wIDDT0orHkHF1mdSm2KHlI50/KTI65dyzP5naZph1ngSf5WIJXSNg/Klu6bHZHhETPQGiUk/sIaOIOF7xTc6dT79PcYFVDniQlaR13izcNUBDEMv/vdclcFs1Grs5FwD38hreM/FuSmDRBqojGq/w8B1H7F3/CnmYitQDCuDRxgXAKMw+CvI371UzDgzrmdvN21aQHSkeILJvGja2KivgEc3eWNE9t3NZFVEw4ImGk6svSVcRYSiqJDXmzdtrjmHBDbGcVoKBpAmSEtsbO+lwiliEzOuNAAQZEOJTu0AV0HOQBKNfe/GlpNcrxgjL+t+vD/Berjw8sk6WAym/rbAPNcS2fzBAiXfbAGAEKTafLasZmw8cp9mpFlT0g1wxNRoiaXcaLum2Si9w7qcVL/VJgThZkwY9AWX2IGO3LWIylwRNeSsG0+jPPG0KSWNT2tWJWTdZGwPNOTBOz2y+OoSFeLjPIhEujdkaX3w== " ]
  root_pass        = "GIHmc5J2xIv2exb"
  image            = "linode/ubuntu22.04"
  private_ip       = true
}
