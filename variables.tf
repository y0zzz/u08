variable "linode_api_token" {
  description = "Linode API Token"
  sensitive = true
}

variable "ssh_key" {}

variable "root_pass" {
  type        = string
  description = "The root password for a system."
}
