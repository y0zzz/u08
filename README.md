# Continuous Deployment with CapRover and GitLab CI

This repository demonstrates a simple setup for continuous deployment using GitLab CI and CapRover.

## Overview

The project includes a basic Python Flask app that says "Hello, this is a simple Flask app deployed with CapRover!". The deployment process is automated through GitLab CI.

## Project Structure

- **app.py**: The main Flask application.
- **Dockerfile**: Dockerfile for building the app container.
- **requirements.txt**: Python dependencies for the Flask app.
- **.gitlab-ci.yml**: GitLab CI configuration for continuous deployment.
- **ansible_playbook.yml**: Ansible playbook for setting up CapRover on servers.
- **README.md**: Detailed instructions on how to use GitLab CI for continuous deployment.

## Getting Started

### 1. Fork and Clone

1. Fork this repository on GitLab.
2. Clone your forked repository:

   ```bash
   git clone https://gitlab.com/y0zzz/u08.git
